from skimage.filters import meijering, sato, frangi, hessian, sobel
import matplotlib.pyplot as plt
from skimage import color
from skimage import data


if __name__ == '__main__':

    def identity(image, **kwargs):
        """Return the original image, ignoring any kwargs."""
        return image

    image = color.rgb2gray(data.retina())[300:700, 700:900]
    cmap = plt.cm.gray

    kwargs = {'sigmas': [1], 'mode': 'reflect'}

    fig, axes = plt.subplots(2, 5)
    for i, black_ridges in enumerate([1, 0]):
        for j, func in enumerate([identity, meijering, sobel, frangi, hessian]):
            kwargs['black_ridges'] = black_ridges
            if func == sobel:
                result = func(image)
            else:
                result = func(image, **kwargs)
            axes[i, j].imshow(result, cmap=cmap, aspect='auto')
            if i == 0:
                axes[i, j].set_title(['Original\nimage', 'Meijering\nneuriteness',
                                      'Sato\ntubeness', 'Frangi\nvesselness',
                                      'Hessian\nvesselness'][j])
            if j == 0:
                axes[i, j].set_ylabel('black_ridges = ' + str(bool(black_ridges)))
            axes[i, j].set_xticks([])
            axes[i, j].set_yticks([])

    plt.tight_layout()
    plt.show()