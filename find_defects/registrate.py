import numpy as np
import cv2

def refine_registrarion(exam_img, ref_img, init_trans=[0.0, 0.0]):
    base_mapper = cv2.reg_MapperGradShift()  # cv2.reg_MapperGradAffine()
    map_pyr = cv2.reg_MapperPyramid(base_mapper)
    map_pyr.numLev_ = 2
    map_pyr.numIterPerScale_ = 8
    reg_map = map_pyr.calculate(img1=exam_img, img2=ref_img, init=cv2.reg_MapShift(init_trans[::-1]))
    map_shift = cv2.reg_MapShift() # reg_MapShift() , reg_MapAffine()
    map_shift.compose(reg_map)
    translation_XY = map_shift.getShift()

    exam_img_shifted = map_shift.inverseWarp(exam_img)

    return exam_img_shifted, translation_XY


def registrate_fb(exam_img, ref_img):
    # Create ORB detector with 5000 features.
    orb_detector = cv2.ORB_create(500)

    # Find keypoints and descriptors.
    kp1, d1 = orb_detector.detectAndCompute(exam_img, None)
    kp2, d2 = orb_detector.detectAndCompute(ref_img, None)

    # Match features between the two images. BF_matcher
    matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = matcher.match(d1, d2)

    # Sort matches on the basis of their Hamming distance.
    matches.sort(key=lambda x: x.distance)

    # Take the top 90 % matches forward.
    matches = matches[:int(len(matches) * 90)]
    no_of_matches = len(matches)

    # Define empty matrices of shape no_of_matches * 2.
    p1 = np.zeros((no_of_matches, 2))
    p2 = np.zeros((no_of_matches, 2))

    for i in range(len(matches)):
        p1[i, :] = kp1[matches[i].queryIdx].pt
        p2[i, :] = kp2[matches[i].trainIdx].pt

    # Find the homography matrix.
    transform, inliers = cv2.estimateAffinePartial2D(p1, p2, refineIters=10000, maxIters=10000, ransacReprojThreshold=0.01, confidence=0.99)
    scale_recovered = np.sqrt(transform[0, 1] * transform[1, 0] + transform[0, 0] * transform[1, 1])
    rot = abs(np.arctan2(transform[0, 1], transform[0, 0]))
    assert (rot < 1e-2) and abs(1.0 - scale_recovered) < 1e-2, "Estimated transformation is wrong"

    # Use this matrix to transform the images
    exam_img_shifted = cv2.warpAffine(src=exam_img, M=transform, dsize=exam_img.shape[::-1])

    # White_mask holds all valid pixels for detection
    white_mask = np.full(exam_img.shape, fill_value=np.float(1))
    white_mask = cv2.warpAffine(src=white_mask, M=transform, dsize=exam_img.shape[::-1])
    white_mask[white_mask < 1.0] = 0.0
    white_mask = white_mask.astype(np.uint8)
    white_mask = cv2.erode(white_mask, np.ones((5, 5), np.uint8), iterations=1)

    return exam_img_shifted, white_mask, transform
