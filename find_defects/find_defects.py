import cv2
from .registrate import registrate_fb
from .multilevel_otsu import multilevel_otsu
from skimage.filters import meijering
import numpy as np
# from skimage.registration import phase_cross_correlation
from skimage.feature import register_translation
from scipy import ndimage as ndi
import matplotlib.pyplot as plt


def calc_ridge(img, sigmas=[1]):
    img_ridge = meijering(img, black_ridges=False, sigmas=sigmas)
    img_ridge[img_ridge <= 0.2] = 0.0
    img_ridge[img_ridge > 0.2] = 1.0
    return img_ridge


def ridges_mask(exam_img, ref_img, transform_exam_img=None, show_overlay=False):
    # This function calculate a ridge img for 2 input images,
    # and the output is a pixel-wise NOR operation between them
    exam_ridge = calc_ridge(exam_img, sigmas=[2])
    ref_ridge = calc_ridge(ref_img, sigmas=[2])
    if transform_exam_img is not None:
        exam_ridge = cv2.warpAffine(src=exam_ridge, M=transform_exam_img, dsize=exam_ridge.shape[::-1])

    if show_overlay:
        rgb_img = np.zeros((exam_ridge.shape[0], exam_ridge.shape[1], 3), dtype=np.uint8)
        rgb_img[:, :, 1] = exam_ridge*255
        rgb_img[:, :, 2] = ref_ridge*255
        plt.imshow(rgb_img)
        plt.show()

    # logical and
    mask = ref_ridge * exam_ridge

    if show_overlay:
        plt.imshow(1.0 - mask)
        plt.show()

    return 1.0 - mask, exam_ridge, ref_ridge


def find_defects(exam_img, ref_img):
    assert exam_img.shape == ref_img.shape, "Input images must be of the same dimensions"

    # Convert to grayscale.
    if len(exam_img.shape) == 3:
        exam_img = cv2.cvtColor(exam_img, cv2.COLOR_BGR2GRAY)
    if len(ref_img.shape) == 3:
        ref_img = cv2.cvtColor(ref_img, cv2.COLOR_BGR2GRAY)

    # TODO
    # white balance

    # images registration
    exam_img_shifted, white_mask, trans = registrate_fb(exam_img, ref_img)

    # refine registration
    shift, _, _ = register_translation(src_image=ref_img,
                                       target_image=exam_img_shifted,
                                       upsample_factor=100,
                                       return_error=True)
    # shift, _, _ = phase_cross_correlation(reference_image=ref_img,
    #                                 moving_image=exam_img_shifted,
    #                                 upsample_factor=100,
    #                                 return_error=True,
    #                                 # reference_mask=white_mask.astype(np.bool)
    #                                 )
    print(f"Detected subpixel offset (y, x): {shift}")
    exam_img_shifted = ndi.shift(exam_img_shifted, shift)

    exam_img_shifted = exam_img_shifted.astype(np.float)
    ref_img = ref_img.astype(np.float)

    # filter by ridges
    ridge_mask, exam_ridge, ref_ridge = ridges_mask(exam_img_shifted, ref_img, show_overlay=False)

    ridge_mask *= white_mask
    exam_ridge *= white_mask

    # diff image
    diff_find_blobs = abs(exam_img_shifted - ref_img)
    diff_find_blobs *= white_mask.astype(np.float)
    diff_find_blobs *= ridge_mask

    # Thresholding, TODO
    thresholds = multilevel_otsu(diff_find_blobs.astype(int), n_classes=5, n_bins=diff_find_blobs.astype(int).max(),
                                 n_classes_used=5, plot_fig=False)
    diff_find_blobs[diff_find_blobs < np.mean(thresholds[-2:-1])] = 0.0

    return ridge_mask, exam_ridge, ref_ridge, diff_find_blobs
