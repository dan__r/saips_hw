import numpy as np

class DefectsList:
    def __init__(self, img_shape=(0, 0), file_path=None):
        self.img_shape = img_shape
        self.defects_list = np.full
        if file_path is not None:
            self.from_file(file_path)

    def from_file(self, file_path):
        pass

    def compare_to_other(self, defects_list_2nd):
        pass
