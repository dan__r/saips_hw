import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from skimage.filters import threshold_multiotsu


def multilevel_otsu(image, n_classes=3, n_bins=256, n_classes_used=256, plot_fig=False):
    # Setting the font size for all plots.
    matplotlib.rcParams['font.size'] = 9

    # Applying multi-Otsu threshold
    thresholds = threshold_multiotsu(image, n_classes, n_bins)

    # Using the threshold values, we generate the n_classes regions.
    regions = np.digitize(image, bins=thresholds[::-1][:n_classes_used])

    if plot_fig:
        fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(10, 3.5))

        # Plotting the original image.
        ax[0].imshow(image, cmap='gray')
        ax[0].set_title('Original')

        # Plotting the histogram and the two thresholds obtained from
        # multi-Otsu.
        ax[1].hist(image.ravel(), bins=255)
        ax[1].set_title('Histogram')
        ax[1].set_yscale('log')
        for thresh in thresholds:
            ax[1].axvline(thresh, color='r')

        # Plotting the Multi Otsu result.
        ax[2].imshow(regions, cmap='Accent')
        ax[2].set_title('Multi-Otsu result')

        plt.show()

    return thresholds
