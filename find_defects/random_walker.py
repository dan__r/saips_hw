import numpy as np
import matplotlib.pyplot as plt

from skimage.segmentation import random_walker
from skimage.data import binary_blobs
from skimage.exposure import rescale_intensity
import skimage


def random_walker_main(img, plot_fig=False):
    img = img.astype(np.float)
    img_vrange = img.max() - img.min()
    markers = np.zeros(img.shape, dtype=np.uint)

    # markers[img < img.min() + 0.05*img_vrange] = 1
    markers[img > (img.max() - 0.5*img_vrange)] = 2

    # Run random walker algorithm
    labels = random_walker(img, markers,
                           beta=5,
                           mode='bf')
    if plot_fig:
        # Plot results
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(8, 3.2),
                                            sharex=True, sharey=True)
        ax1.imshow(img, cmap='gray')
        ax1.axis('off')
        ax1.set_title('Noisy img')
        ax2.imshow(markers, cmap='magma')
        ax2.axis('off')
        ax2.set_title('Markers')
        ax3.imshow(labels, cmap='gray')
        ax3.axis('off')
        ax3.set_title('Segmentation')

        fig.tight_layout()
        plt.show()


if __name__ == '__main__':
    # Generate noisy synthetic data
    img = skimage.img_as_float(binary_blobs(length=128, seed=1))
    sigma = 0.35
    img += np.random.normal(loc=0, scale=sigma, size=img.shape)
    img = rescale_intensity(img, in_range=(-sigma, 1 + sigma),
                             out_range=(-1, 1))
    random_walker_main(img)