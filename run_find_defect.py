import cv2
import numpy as np
import pathlib

from find_defects.find_defects import find_defects
from matplotlib import pyplot as plt
# from find_defects.random_walker import random_walker_main


def show_output(exam_img, ref_img, diff_find_blobs, ridge_mask, case_num):
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(10, 10))

    im = ax[0, 0].imshow(ref_img, cmap='gray')
    plt.colorbar(im, ax=ax[0, 0])
    ax[0, 0].set_title('ref_img, case#{}'.format(case_num))

    im = ax[0, 1].imshow(exam_img, cmap='gray')
    plt.colorbar(im, ax=ax[0, 1])
    ax[0, 1].set_title('exam_img, case#{}'.format(case_num))

    im = ax[1, 0].imshow(diff_find_blobs, cmap='Greens_r')
    plt.colorbar(im, ax=ax[1, 0])
    ax[1, 0].set_title('bitmap, revA, case#{}'.format(case_num))

    ax[1, 1].imshow(ridge_mask, cmap='gray')
    ax[1, 1].set_title('valid mask, case#{}'.format(case_num))

    plt.show()
    fig.savefig("output_{}.jpg".format(case_num))


if __name__ == '__main__':
    fpath = pathlib.Path(__file__).parent.absolute()
    case_num = 3  # 1 | 2 | 3
    for case_num in np.arange(1, 4):
        if case_num == 3:
            non_def = "non_"
        else:
            non_def = ""
        exam_img = cv2.imread("{}/images/{}defective_examples/case{}_inspected_image.tif".format(fpath, non_def, case_num))  # Examined image
        ref_img = cv2.imread("{}/images/{}defective_examples/case{}_reference_image.tif".format(fpath, non_def, case_num))  # reference image

        if len(exam_img.shape) == 3:
            exam_img = cv2.cvtColor(exam_img, cv2.COLOR_BGR2GRAY)
        if len(ref_img.shape) == 3:
            ref_img = cv2.cvtColor(ref_img, cv2.COLOR_BGR2GRAY)

        ridge_mask, exam_ridge, ref_ridge, diff_find_blobs = find_defects(exam_img, ref_img)

        show_output(exam_img, ref_img, diff_find_blobs, ridge_mask, case_num)
